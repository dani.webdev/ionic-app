import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { TabsPage} from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { HomePage, PopoverPage } from "../pages/home/home";
import { TransactionPage } from "../pages/transaction/transaction";
import { WelcomePage } from "../pages/welcome/welcome";
import { RegisterPage } from "../pages/welcome/welcome";
import { AccountPage } from "../pages/account/account";

import { TrxHistoryPageModule } from "../pages/trx-history/trx-history.module";
@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    PopoverPage,
    TransactionPage,
    TabsPage,
    WelcomePage,
    RegisterPage,
    AccountPage
    
  ],
  imports: [
    TrxHistoryPageModule,
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    PopoverPage,
    TransactionPage,
    TabsPage,
    RegisterPage,
    WelcomePage,
    AccountPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
