import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { LoginPage } from "../login/login";
/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  
  LoginPage = () => {
    this.navCtrl.setRoot(LoginPage, [], {
      animate:true,
      direction: "forward" // or "backward"
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
  }

}


@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    
  }

}
