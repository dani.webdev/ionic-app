import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, Tabs} from 'ionic-angular';


import { HomePage } from "../home/home";
import { AccountPage } from "../account/account";
import { TrxHistoryPage } from "../trx-history/trx-history";
/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = TrxHistoryPage;
  tab3Root = HomePage;
  tab4Root = AccountPage;

  @ViewChild("paymentTabs") paymentTabs: Tabs;
  @ViewChild(Slides) slides: Slides;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

  slideChange(){
    //console.log(this.slides.getActiveIndex());
     if(this.slides.getActiveIndex() == 0){
        this.paymentTabs.select(0);
        this.slides.slideTo(0);
      }
      if(this.slides.getActiveIndex() == 1){
        this.paymentTabs.select(1);
        this.slides.slideTo(1);
      }
      if(this.slides.getActiveIndex() == 2){
        this.paymentTabs.select(2);
        this.slides.slideTo(2);
      }
  }

  tabChange(){
    //console.log("tabchange");
    //console.log(this.paymentTabs.getSelected().index);
    if(this.paymentTabs.getSelected().index == 0){
      this.paymentTabs.select(0);
      this.slides.slideTo(0);
    }
    if(this.paymentTabs.getSelected().index == 1){
      this.paymentTabs.select(1);
      this.slides.slideTo(1);
    }
    if(this.paymentTabs.getSelected().index == 2){
      this.paymentTabs.select(2);
      this.slides.slideTo(2);
    }
  }

}
