import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrxHistoryPage } from './trx-history';

@NgModule({
  declarations: [
    TrxHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(TrxHistoryPage),
  ],
})
export class TrxHistoryPageModule {}
